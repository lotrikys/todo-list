from django.urls import path

from main.views import IndexView, TaskCreateView, TagsView, TagsCreateView, task_done, TaskUpdateView, TaskDeleteView, \
    TagsUpdateView, TagsDeleteView

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("task/create/", TaskCreateView.as_view(), name="create-task"),
    path("tags/", TagsView.as_view(), name="tags-view"),
    path("tags/create/", TagsCreateView.as_view(), name="tags-create"),
    path("tags/update/<int:pk>", TagsUpdateView.as_view(), name="tags-update"),
    path("tags/delete/<int:pk>", TagsDeleteView.as_view(), name="tags-delete"),
    path("task/done/<int:pk>", task_done, name="task-done"),
    path("task/update/<int:pk>", TaskUpdateView.as_view(), name="task-update"),
    path("task/delete/<int:pk>", TaskDeleteView.as_view(), name="task-delete"),
]

app_name = "todo"
