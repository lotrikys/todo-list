from django import forms

from main.models import Task


class TaskUpdateForm(forms.ModelForm):
    deadline = forms.DateField(
        label="Deadline",
        required=False,
        widget=forms.widgets.DateInput(
            format="%Y-%m-%d",
            attrs={"class": "form-control", "type": "date"}
        )
    )

    class Meta:
        model = Task
        fields = "__all__"
