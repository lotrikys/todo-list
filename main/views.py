from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic

from main.forms import TaskUpdateForm
from main.models import Task, Tags


class IndexView(generic.ListView):
    model = Task
    context_object_name = "tasks"
    template_name = "main/index.html"


class TaskCreateView(generic.CreateView):
    model = Task
    form_class = TaskUpdateForm
    success_url = reverse_lazy("todo:index")


def task_done(request, pk):
    task = Task.objects.get(id=pk)

    task.is_done = not task.is_done
    task.save()

    return HttpResponseRedirect(reverse_lazy("todo:index"))


class TaskUpdateView(generic.UpdateView):
    model = Task
    form_class = TaskUpdateForm
    success_url = reverse_lazy("todo:index")


class TaskDeleteView(generic.DeleteView):
    model = Task
    template_name = "main/task_delete_confirm.html"
    success_url = reverse_lazy("todo:index")


class TagsView(generic.ListView):
    model = Tags
    context_object_name = "tags"
    template_name = "main/tags.html"


class TagsCreateView(generic.CreateView):
    model = Tags
    fields = "__all__"
    success_url = reverse_lazy("todo:tags-view")


class TagsUpdateView(generic.UpdateView):
    model = Tags
    fields = "__all__"
    success_url = reverse_lazy("todo:tags-view")


class TagsDeleteView(generic.DeleteView):
    model = Tags
    template_name = "main/tag_delete_confirm.html"
    success_url = reverse_lazy("todo:tags-view")
