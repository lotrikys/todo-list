from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    deadline = models.DateField(null=True, blank=True)
    is_done = models.BooleanField()
    tags = models.ManyToManyField("Tags", related_name="tasks")


class Tags(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
